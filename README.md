# **Repository Front End Web Evolty 2020**

<figure>
    <img src="image/evolty.png" width="50%" style="position:relative; left: 50%; transform:translateX(-50%);">
</figure>

<br>

## Jika ingin merubah file css, mohon untuk tidak langsung di `style_s.css` melainkan melalui `style_s.scss`.
## Agar tidak terjadi error saat compiling file scss nya

## Apabila ingin langsung mengedit bisa melalui `styles.css`.

<br>

### **Semangat, semoga Evolty 2020 bisa <span style="color: #4971aa">*Sukses dan Lancar*</span> :)**
